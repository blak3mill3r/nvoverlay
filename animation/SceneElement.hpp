#ifndef __SCENE_ELEMENT_HPP__
#define __SCENE_ELEMENT_HPP__

#include <QPainter>
#include <QMatrix4x4>
#include <QStaticText>
#include <QSvgRenderer>
#include <iostream>

struct SceneElement {
  virtual ~SceneElement() = default;
  virtual void paint(QPainter &, QMatrix4x4) = 0;
protected:
  SceneElement() {}
};

struct TextSceneElement: virtual SceneElement {
  QString text;
  QFont font;
  QPen pen;
  QStaticText text_layout;
  TextSceneElement(QString, QString, int);
  virtual void paint(QPainter &, QMatrix4x4);
};

struct SvgSceneElement: virtual SceneElement {
  QSvgRenderer renderer;
  QFont font;
  QPen pen;
  QStaticText text_layout;
  SvgSceneElement(QString);
  virtual void paint(QPainter &, QMatrix4x4);
};

#endif
