#ifndef __SCENE_ELEMENT_HPP__
#define __SCENE_ELEMENT_HPP__

#include <QPainter>
#include <QMatrix4x4>
#include <QStaticText>
#include <QSvgRenderer>
#include <iostream>

#include "server.hpp"

struct SceneElement {
  virtual ~SceneElement() = default;
  virtual void paint(QPainter &, QMatrix4x4) {}
  //virtual void step(int);
  SceneElement(anim_tween_t);
  anim_tween_t tween;
  QMatrix4x4 model_matrix;
  double scale, rotation, translateX, translateY;
  bool finished;
};

struct TextSceneElement: virtual SceneElement {
  QString text;
  QFont font;
  QPen pen;
  QStaticText text_layout;
  TextSceneElement(anim_tween_t, QString, QString, int);
  virtual void paint(QPainter &, QMatrix4x4);
};

struct SvgSceneElement: virtual SceneElement {
  QSvgRenderer renderer;
  QFont font;
  QPen pen;
  QStaticText text_layout;
  SvgSceneElement(anim_tween_t, QString);
  virtual void paint(QPainter &, QMatrix4x4);
};

#endif
