#ifndef __SCENE_GRAPH_HPP__
#define __SCENE_GRAPH_HPP__

#include <QPainter>
#include <QMatrix4x4>
#include <QStaticText>
#include <QSvgRenderer>
#include <functional>
#include <vector>
#include <memory>
#include <algorithm>
#include "animation/SceneElement.hpp"

#include "tweeny.h"

enum Easing {ease_linear, ease_cubic_out, ease_back_in, ease_back_out};

typedef std::tuple<double,double,double,double,Easing,int> anim_values_t;
typedef tweeny::tween<double,double,double,double> anim_tween_t;

typedef unsigned int node_id_t;


class SceneNode {
public:
  SceneNode();
  void addChild(std::shared_ptr<SceneNode> child);
  void deleteChild(node_id_t id);

  void setElement(std::unique_ptr<SceneElement> elem);
  void setTween(anim_tween_t _tween);

  void paint(QPainter &p, QMatrix4x4 parentTransform);

  bool step(int deltaTime);

  QMatrix4x4 model_matrix;
  anim_tween_t tween;
  bool finished, is_animated;

  void setId(node_id_t);
  node_id_t getId();

private:
  node_id_t id;

  std::vector<std::shared_ptr<SceneNode>> children;
  std::unique_ptr<SceneElement> element;
};

#endif
