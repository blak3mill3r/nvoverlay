#include "SceneGraph.hpp"

SceneNode::SceneNode() :
  finished(false), is_animated(false), id(0), element(nullptr) {
}

void SceneNode::addChild(std::shared_ptr<SceneNode> child) {
  children.push_back(child);
}

void SceneNode::setId(node_id_t id) {
  this->id = id;
}

node_id_t SceneNode::getId() {
  return id;
}

void SceneNode::deleteChild(node_id_t id) {
  children.erase(std::remove_if(children.begin(), children.end(),
                                [id](const std::shared_ptr<SceneNode>& child) {
                                  //  std::cout << "comparing " << child->id << " and its " << (child->id == id) << std::endl;
                                  return child && child->getId() == id;
                                }),
                 children.end());
}

void SceneNode::setTween(anim_tween_t _tween) {
  is_animated=true;
  finished=false;
  tween = _tween;

  tween.onStep([this](tweeny::tween<double,double,double,double> & t, double scale, double rotation, double tx, double ty) {
    model_matrix.setToIdentity();
    model_matrix.translate(tx, ty, 0);
    model_matrix.scale(scale);
    model_matrix.rotate(rotation, 0, 0, 1);

    if (t.progress() >= 1.0f) {
      finished = true;
      return true; // causes tweeny to discard this callback
    } else return false;
  });
}

void SceneNode::setElement(std::unique_ptr<SceneElement> elem) {
  element = std::move(elem);
}

void SceneNode::paint(QPainter &p, QMatrix4x4 parentTransform) {
  QMatrix4x4 m = parentTransform * model_matrix;
  if (element)               element->paint(p, m);
  for (auto& child : children) child->paint(p, m);
}

bool SceneNode::step(int ms) {

  if(is_animated) tween.step(ms);

  // remove_if moves the elements for which the predicate passes to the end, and returns an iterator to the first of those elements
  children.erase(std::remove_if(children.begin(), children.end(), std::bind(&SceneNode::step, std::placeholders::_1, ms)),
                 children.end());

  // return false to remove this node if it's done animating
  return
    children.empty() && is_animated && tween.progress() >= 1.0f ;
}
