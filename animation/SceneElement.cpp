#include "SceneElement.hpp"

SceneElement::SceneElement(anim_tween_t _tween)
  : tween(_tween), finished(false) {
  tween.onStep([this](tweeny::tween<double,double,double,double> & t, double _scale, double _rotation, double _tx, double _ty) {
    scale = _scale;
    rotation = _rotation;
    translateX = _tx;
    translateY = _ty;

    if (t.progress() >= 1.0f) {
      finished = true;   // causes the element to be removed next frame
      return true;       // discard the callback
    } else return false; // keep the callback
  });
};

TextSceneElement::TextSceneElement(anim_tween_t _tween, QString _text, QString _font, int _point_size)
  : SceneElement(_tween),
    text(_text),
    font(QFont(_font, _point_size)),
    pen(QPen(Qt::white)),
    text_layout(_text) {

};

void TextSceneElement::paint(QPainter &p, QMatrix4x4 z) {

  //std::cout << "Okay paint me" << std::endl;
  model_matrix.setToIdentity();
  model_matrix.translate(translateX, translateY, 0);
  model_matrix.scale(scale);
  model_matrix.rotate(rotation, 0, 0, 1);

  QTransform text_transform = (z * model_matrix).toTransform();

  p.setTransform(text_transform, false);
  p.setPen(pen);
  p.setFont(font);

  text_layout.prepare(text_transform, font);
  text_layout.setTextOption(QTextOption(Qt::AlignCenter));
  qreal x = - (text_layout.size().width() / 2);
  qreal y = - (text_layout.size().height() / 2);
  p.drawStaticText(x, y, text_layout);
}


// tweens...
// they get an onStep() callback which binds members and updates them
// that could be in SceneElement...?

// constructing the tween from json is currently very rudimentary
// and is done in the command server

// whatever, you need a way to set a tween
// and for now it can always be a tween with scale,rotation,translateX,translateY

SvgSceneElement::SvgSceneElement(anim_tween_t _tween, QString _file)
  : SceneElement(_tween),
    renderer(_file)
{};


void SvgSceneElement::paint(QPainter &p, QMatrix4x4 z) {
  // centered at lower right
  //QRect svgr(-1920,-2160,1920,2160);

  // centered at upper left
  //QRect svgr(0,0,1920,2160);

  // centered at center, but why the fuck?
  // I do not get it
  QRect svgr(-960,-1080,1920,2160);

  //duplication!
  model_matrix.setToIdentity();
  model_matrix.translate(translateX, translateY, 0);
  model_matrix.scale(scale);
  model_matrix.rotate(rotation, 0, 0, 1);
  QTransform transform = (z * model_matrix).toTransform();

  p.setTransform(transform, false);
  // end duplication!

  //std::cout << renderer.framesPerSecond() << std::endl;;
  //std::cout << renderer.animated() <<  std::endl;

  renderer.render(&p,svgr);
}
