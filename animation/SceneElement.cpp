#include "SceneElement.hpp"

TextSceneElement::TextSceneElement(QString _text, QString _font, int _point_size)
  : text(_text),
    font(QFont(_font, _point_size)),
    pen(QPen(Qt::white)),
    text_layout(_text) {

};

void TextSceneElement::paint(QPainter &p, QMatrix4x4 mm) {

  //std::cout << "Okay paint me" << std::endl;
  //model_matrix.setToIdentity();
  //model_matrix.translate(translateX, translateY, 0);
  //model_matrix.scale(scale);
  //model_matrix.rotate(rotation, 0, 0, 1);

  QTransform text_transform = mm.toTransform();

  p.setTransform(text_transform, false);
  p.setPen(pen);
  p.setFont(font);

  text_layout.prepare(text_transform, font);
  text_layout.setTextOption(QTextOption(Qt::AlignCenter));
  qreal x = - (text_layout.size().width() / 2);
  qreal y = - (text_layout.size().height() / 2);
  p.drawStaticText(x, y, text_layout);
}

SvgSceneElement::SvgSceneElement(QString _file)
  : renderer(_file)
{};


void SvgSceneElement::paint(QPainter &p, QMatrix4x4 mm) {
  // centered at lower right
  //QRect svgr(-1920,-2160,1920,2160);

  // centered at upper left
  //QRect svgr(0,0,1920,2160);

  // FIXME hard-coded pixel values
  // it is not clear to me how to compute these from width & height
  // I just adjusted these until it appeared roughly centered
  QRect svgr(-960,-1080,1920,2160);

  //duplication!
  //model_matrix.setToIdentity();
  //model_matrix.translate(translateX, translateY, 0);
  //model_matrix.scale(scale);
  //model_matrix.rotate(rotation, 0, 0, 1);
  QTransform transform = mm.toTransform();

  p.setTransform(transform, false);
  // end duplication!

  //std::cout << renderer.framesPerSecond() << std::endl;;
  //std::cout << renderer.animated() <<  std::endl;

  renderer.render(&p,svgr);
}
