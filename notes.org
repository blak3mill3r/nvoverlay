** future musings
    * perhaps it should be possible for nv to know when an animation completes
      and/or to queue multiple animations so they are composable
      that is not possible right now, although the scene graph allows you to make sub-animations,
      you cannot compose two animations into a larger animation with one after the other
    * the only way for nv to know when the animation is complete right now
      is to use a timer in clojure; count down from sending the /animate request
      thus far I haven't actually wanted to do a chain of animations or anything else like that
*** things to add to animation capabilities
    * catmull-rom splines or bsplines?
      combined with tweens... since a spline is only a path and not a f(t)...
      or is it?
      even if it is you should be able to compose a tween with that f(t)
** json animation descriptors
*** splines and tweens
    * the tween is just composed with the time input before passing it to the spline function
      * p(t) = spline(tween(t))
    * the parameters can each be treated independently
    * all must be defined at the ends
    * and in addition you need 2 extra spline control points that aren't part of the path
      * to control the curvature of the path at the start/end
      * if you ignore that then it always starts and ends going straight away from the point
      * this makes the json description less obvious
      * because without that, you could specify spline control value along with tween and keyframe?
** Would be nice if I could make it just 1 thread, with fibers
   * https://www.boost.org/doc/libs/1_67_0/libs/fiber/doc/html/fiber/integration/event_driven_program.html
   * I did this with mouser and xlib, can I do it with Qt?
     * only thing is, Qt expects me to call app.exec() which blocks main()
     * how can I integrate boost::fiber's loop?
     * well, what happens with app.exec() exactly?
     * from docs:
       * In general, the trick is to arrange to pass control to this_fiber::yield() frequently. You could use an Asio timer for that purpose. You could instantiate the timer, arranging to call a handler function when the timer expires. The handler function could call yield(), then reset the timer and arrange to wake up again on its next expiration.
       * so... I could just set a Qt timer to repeatedly call this_fiber::yield()
     * then I can have animation using fibers potentially
       * but you'd also have to have the http server be fiber-based
       * well, can do it a step at a time... the fiber channels are good enough to just block the thread if they're not called from a fiber
         * this is how it is working now, there's an http server thread and it blocks putting to a channel
     * if you could have fibers and channels, would that be any good for animations?
       * maybe... stringing together animations; put to a channel when animation is complete
**  nvidia + compton + prime + nvoverlay: uh oh
    * black screen, can't get nvoverlay to be an overlay
    * maybe this option
https://forums.developer.nvidia.com/t/understanding-nvidia-drm-modeset-1-nvidia-linux-driver-modesetting/204068
** physics?
    * https://bixense.com/chipmunkpp/
    * the underlying c library libchipmunk has some kinda support for taking an image and auto-physics-bodying it
    * that could be cool (the text in an emacs buffer having things roll off of it)
    * also, just taking the window geometry and making bodies out of that could be fun
      * even though they are rectangles
** tweeny master has a new feature that I ought to use; resolving easings from strings
    * adds easing enums and string resolution
     03/10 16:48 blake@bwip: ~/w/nvoverlay/deps/tweeny
     --------------------------------------
     git show 4a64290fb7356611bdb6a7066e1eefd367e413dd
    * since I needed this for the JSON animation descriptors, I had to do it myself, but should now switch to using tweeny's
