#ifndef __COMMAND_SERVER_HPP
#define __COMMAND_SERVER_HPP

#include <httplib.h>
#include <iostream>
#include <iomanip>
#include <vector>
#include <string>
#include <stdexcept>
#include <nlohmann/json.hpp>
#include <boost/fiber/all.hpp>

#include "tweeny.h"
#include "animation/SceneGraph.hpp"

typedef boost::fibers::buffered_channel< nlohmann::json > command_channel_t;

class CommandServer {
public:
  CommandServer();
  //~CommandServer();
  void operator()(int port, command_channel_t *_chan);
  static std::shared_ptr<SceneNode> createNodeFromJSON(const nlohmann::json& j);

private:
  httplib::Server *svr;
  static Easing find_easing(std::string n);
};

#endif
