#ifndef __COMMAND_SERVER_HPP
#define __COMMAND_SERVER_HPP

#include <httplib.h>
#include <iostream>
#include <iomanip>
#include <vector>
#include <string>
#include <stdexcept>
#include <nlohmann/json.hpp>
#include <boost/fiber/all.hpp>

#include "tweeny.h"

enum Easing {ease_linear, ease_cubic_out, ease_back_in};

typedef std::tuple<double,double,double,double,Easing,int> anim_values_t;
typedef tweeny::tween<double,double,double,double> anim_tween_t;

struct Command {
  std::string message;
  std::string svg_file;
  std::vector<anim_values_t> anim_values;
  anim_tween_t getTween();
};

typedef boost::fibers::buffered_channel< Command > command_channel_t;

class CommandServer {
public:
  CommandServer();
  //~CommandServer();
  void operator()(int port, command_channel_t *_chan);

private:
  httplib::Server *svr;
  static Easing find_easing(std::string n);
  static Command interpret_command(nlohmann::json j);
};

#endif
