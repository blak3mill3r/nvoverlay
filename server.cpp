#include "server.hpp"

using namespace httplib;
using json = nlohmann::json;

anim_tween_t Command::getTween() {
  auto tw = tweeny::from(std::get<0>(anim_values[0]), std::get<1>(anim_values[0]), 200.0, 200.0);
  for(auto& e : anim_values) {
    // 0, 1, 2, 3 are scale, rotate, tx, ty
    // 4 is easing
    // 5 is ms
    tw.to(std::get<0>(e), std::get<1>(e),std::get<2>(e),std::get<3>(e)).during(std::get<5>(e));
    switch(std::get<4>(e)) {
    case ease_cubic_out: tw.via(tweeny::easing::cubicOut); break;
    case ease_linear: tw.via(tweeny::easing::linear); break;
    case ease_back_in: tw.via(tweeny::easing::backIn); break;
    }
  }
  return tw;
}

CommandServer::CommandServer()
  : svr(new Server)
{
  if (!svr->is_valid()) {
    std::cerr << "CommandServer has an error..." << std::endl;
  }
}

// wtf, this destructor gets called immediately? from main()?
//CommandServer::~CommandServer()
//{
//  delete svr;
//  std::cout << "CommandServer destructor" << std::endl;
//}

Easing CommandServer::find_easing(std::string n){
  Easing easing;
       if(n=="cubic_out") easing = ease_cubic_out;
  else if(n=="linear")    easing = ease_linear;
  else if(n=="back_in")   easing = ease_back_in;
  else throw std::invalid_argument("Bad easing name, try again.");
  return easing;
}

Command CommandServer::interpret_command(json j) {
  // std::cout << std::setw(2) << j << std::endl;
  Command c;
  c.message = c.svg_file = "";
  if (j["message"].is_string()) c.message  = j["message"];
  if (j["svg_file"].is_string()) c.svg_file = j["svg_file"];
  for (auto& e : j["keyframes"]) {
    double scale  = e.at(0);
    double rotate = e.at(1);
    double tx     = e.at(2);
    double ty     = e.at(3);
    Easing easing = find_easing(e.at(4));
    int    ms     = e.at(5);
    c.anim_values.push_back({ scale,rotate,tx,ty,easing,ms });
    //c.anim_values.push_back(std::make_tuple( scale,rotate,easing,ms ));
  }
  return c;
}

void CommandServer::operator()(int port, command_channel_t *chan) {
  svr->Post("/animate", [chan](const Request & req, Response &res) {

    json j;

    try {
      j = json::parse(req.body);
    } catch (std::exception &e) {
      res.status = 400;
      res.set_content("{\"success\": false, \"reason\": \"JSON failed to parse\"}\n", "text/json");
      return;
    }

    try {
      chan->push(interpret_command(j));
      res.set_content("{\"success\": true}\n", "text/json");
    } catch (std::exception &e) {
      res.status = 400;
      res.set_content("{\"success\": false, \"reason\": \"Command is invalid\"}\n", "text/json");
      return;
    }

  });

  svr->listen("localhost", port);
  std::cerr << "listen() has returned" << std::endl;
}

/*
curl --location --request POST 'http://localhost:9112/animate' \
--header 'Content-Type: application/json' \
--data-raw '{"foo":3}'

*/

