#include "server.hpp"

using namespace httplib;
using json = nlohmann::json;

CommandServer::CommandServer()
  : svr(new Server)
{
  if (!svr->is_valid()) {
    std::cerr << "CommandServer has an error..." << std::endl;
  }
}

// wtf, this destructor gets called immediately? from main()?
//CommandServer::~CommandServer()
//{
//  delete svr;
//  std::cout << "CommandServer destructor" << std::endl;
//}

Easing CommandServer::find_easing(std::string n){
  // FIXME why am I duplicating the bit about resolving easings from strings?
  // also, tweeny now provides that itself
  Easing easing;
       if(n=="cubic_out") easing = ease_cubic_out;
  else if(n=="linear")    easing = ease_linear;
  else if(n=="back_in")   easing = ease_back_in;
  else if(n=="back_out")   easing = ease_back_out;
  else throw std::invalid_argument("Bad easing name, try again.");
  return easing;
}

std::shared_ptr<SceneNode> CommandServer::createNodeFromJSON(const json& j) {
  auto node = std::make_shared<SceneNode>();
  anim_tween_t tw;

  if (j.contains("id")) node->setId((node_id_t)j["id"]);

  // Set up the element for the node
  if (j.find("svg_file") != j.end()) {
    node->setElement(std::make_unique<SvgSceneElement>(QString::fromStdString(j["svg_file"].get<std::string>())));
  } else if (j.find("message") != j.end()) {
    node->setElement(std::make_unique<TextSceneElement>(QString::fromStdString(j["message"].get<std::string>()), "JetBrainsMono", 60));
  }

  // Handle keyframes if present
  if (j.find("keyframes") != j.end() && j["keyframes"].size() > 1) {
    // Use the first keyframe for initial values
    tw = tweeny::from((double)j["keyframes"][0].at(0),
                      (double)j["keyframes"][0].at(1),
                      (double)j["keyframes"][0].at(2),
                      (double)j["keyframes"][0].at(3));

    // Process the rest of the keyframes
    for (size_t i = 1; i < j["keyframes"].size(); ++i) {
      auto& e = j["keyframes"][i];
      double next_scale  = e.at(0);
      double next_rotate = e.at(1);
      double next_tx     = e.at(2);
      double next_ty     = e.at(3);
      Easing easing = find_easing(e.at(4));
      int    ms     = e.at(5);

      tw.to(next_scale, next_rotate, next_tx, next_ty).during(ms);

      // this can be improved by using tweeny's new string-resolution feature
      // see commit 4a64290fb7356611bdb6a7066e1eefd367e413dd in tweeny
      switch(easing) {
      case ease_cubic_out: tw.via(tweeny::easing::cubicOut); break;
      case ease_linear: tw.via(tweeny::easing::linear); break;
      case ease_back_in: tw.via(tweeny::easing::backIn); break;
      case ease_back_out: tw.via(tweeny::easing::backOut); break;
      }
    }
    node->setTween(tw);
  }

  // Handle child nodes
  if (j.find("containing") != j.end()) {
    for (const auto& child : j["containing"]) {
      node->addChild(createNodeFromJSON(child));
    }
  }

  return node;
}

void CommandServer::operator()(int port, command_channel_t *chan) {
  svr->Post("/animate", [chan](const Request & req, Response &res) {

    json j;

    try {
      j = json::parse(req.body);
    } catch (std::exception &e) {
      res.status = 400;
      res.set_content("{\"success\": false, \"reason\": \"JSON failed to parse\"}\n", "text/json");
      return;
    }

    try {
      chan->push(j);
      res.set_content("{\"success\": true}\n", "text/json");
    } catch (std::exception &e) {
      res.status = 400;
      res.set_content("{\"success\": false, \"reason\": \"Command is invalid\"}\n", "text/json");
      return;
    }

  });

  svr->listen("localhost", port);
  std::cerr << "listen() has returned" << std::endl;
}

/*
curl --location --request POST 'http://localhost:9112/animate' \
--header 'Content-Type: application/json' \
--data-raw '{"foo":3}'

*/
