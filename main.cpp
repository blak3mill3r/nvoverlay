#include <QOpenGLWindow>
#include <QSvgRenderer>

#include <QScreen>
#include <QPainter>
//#include <QPainterPath>
//#include <QApplication>
#include <QGuiApplication>
#include <QMatrix4x4>
#include <QStaticText>
#include <QKeyEvent>
#include <QElapsedTimer>
#include <QtMath>
//#include <QDesktopWidget>

#include <thread>

#include <iostream>
#include <iomanip>


#include <boost/ptr_container/ptr_list.hpp>


#include "tweeny.h"
#include "nvoverlay.h"
#include "server.hpp"
#include "animation/SceneElement.hpp"

#include <nlohmann/json.hpp>

using json = nlohmann::json;
using tweeny::easing;

using namespace std;
using namespace boost;
using namespace boost::fibers;
using namespace boost::this_fiber;

/*
static QPainterPath painterPathForTriangle()
{
  static const QPointF bottomLeft(-1.0, -1.0);
  static const QPointF top(0.0, 1.0);
  static const QPointF bottomRight(1.0, -1.0);

  QPainterPath path(bottomLeft);
  path.lineTo(top);
  path.lineTo(bottomRight);
  path.closeSubpath();
  return path;
}
*/

class OverlayWindow : public QOpenGLWindow
{
  Q_OBJECT

public:
  OverlayWindow(QRect);
  void command_handler();

protected:
  void paintGL() override;
  void resizeGL(int w, int h) override;
  bool eventFilter(QObject *,QEvent *) override;
  //  void keyPressEvent(QKeyEvent *e) override;

private:

  void setAnimating(bool enabled);

  QMatrix4x4 m_window_painter_matrix;
  QMatrix4x4 m_projection;
  QMatrix4x4 m_view;

  FragmentToy m_fragment_toy;
  QStaticText m_text_layout;

  bool m_animate;

  QElapsedTimer timer;

  qint64 last_frame_time;

  boost::ptr_list<SceneElement> scene;
};

// Use NoPartialUpdate. This means that all the rendering goes directly to
// the window surface, no additional framebuffer object stands in the
// middle. This is fine since we will clear the entire framebuffer on each
// paint. Under the hood this means that the behavior is equivalent to the
// manual makeCurrent - perform OpenGL calls - swapBuffers loop that is
// typical in pure QWindow-based applications.
OverlayWindow::OverlayWindow(QRect size)
  : QOpenGLWindow(QOpenGLWindow::NoPartialUpdate)
  , m_fragment_toy("./background.frag")
    //, m_text_layout("nerdvana")
  , m_text_layout(";)")
  , m_animate(true)
{
  // FIXME
  // I don't know how to make xmonad set the size and then doIgnore
  //  that may not be possible? since doIgnore breaks doFloat with a custom rect
  setGeometry(0, 0, 3840, 4320);
  // half screen width and height, centered, performs better?
  //setGeometry(0, 0, size.width(), size.height());
  //setX(960);
  //setY(480);

  resizeGL(3840, 4320);

  m_view.lookAt(QVector3D(0,0,1),
                QVector3D(0,0,0),
                QVector3D(0,1,0));

  setAnimating(m_animate);

  last_frame_time = 0;
  timer.start();

}

bool OverlayWindow::eventFilter(QObject *, QEvent *)
{
  return false;
}

command_channel_t command_channel{16};

void OverlayWindow::paintGL()
{

  qint64 elapsed_ms = timer.elapsed();

  // hack to keep it on top of emacs... why?
  static int dong = 0;
  if(dong++ % 10 == 0) raise();

  //m_fragment_toy.draw(size());

  glDisable(GL_STENCIL_TEST);
  glDisable(GL_DEPTH_TEST);

  glClearColor(0, 0, 0, 0);
  glClear(GL_COLOR_BUFFER_BIT);

  QPainter p(this);

  //QMatrix4x4 mvp = m_projection * m_view * m_model_triangle;
  //p.setTransform(mvp.toTransform(), true);

  //QRectF rec(-1, -1, 2, 2);

  //QRadialGradient gradient1(rec.center(), rec.width()/8);
  //gradient1.setColorAt(.0,Qt::blue);
  //gradient1.setColorAt(.9, Qt::transparent);
  //p.fillPath(painterPathForTriangle(), QBrush(gradient1));

  int frame_time_ms = elapsed_ms - last_frame_time;

  // step the tween for each scene element
  QMatrix4x4 view_matrix = m_window_painter_matrix * m_view;

  //std::cout << "Have this many = " << scene.size() <<  std::endl;
  for (auto it = scene.begin(); it != scene.end(); ) {
    it->tween.step(frame_time_ms);
    if(it->finished) it = scene.erase(it);
    else {
      //std::cout << "Painting an element" << std::endl;
      it->paint(p, view_matrix);
      ++it;
    }
  }

  last_frame_time = elapsed_ms;

  // let the fiber manager have a go...
  yield();
}

void OverlayWindow::resizeGL(int w, int h)
{
  m_window_painter_matrix.setToIdentity();
  m_window_painter_matrix.translate(w / 2.0, h / 2.0);
  m_window_painter_matrix.scale(1.0, 0.8888888888888888);

  //m_text_layout.setTextWidth(max(w * 0.8, 80.0));
  m_text_layout.setTextWidth(1800); // FIXME hard-coded width
  //m_text_layout.setTextFormat(Qt::RichText);

  m_projection.setToIdentity();
  m_projection.perspective(45.f, qreal(w) / qreal(h), 0.1f, 100.f);
}

void OverlayWindow::setAnimating(bool enabled)
{
  if (enabled) {
    // Animate continuously, throttled by the blocking swapBuffers() call the
    // QOpenGLWindow internally executes after each paint. Once that is done
    // (frameSwapped signal is emitted), we schedule a new update. This
    // obviously assumes that the swap interval (see
    // QSurfaceFormat::setSwapInterval()) is non-zero.
    connect(this, &QOpenGLWindow::frameSwapped,
            this, QOverload<>::of(&QPaintDeviceWindow::update));
    update();
  } else {
    disconnect(this, &QOpenGLWindow::frameSwapped,
               this, QOverload<>::of(&QPaintDeviceWindow::update));
  }
}

void OverlayWindow::command_handler()
{
  Command command;
  for(auto pop = command_channel.pop(command); pop == channel_op_status::success; pop = command_channel.pop(command)) {
    if(command.message != "")
      scene.push_back(new TextSceneElement(command.getTween(), QString::fromStdString(command.message), "JetBrainsMono", 60));
    else if(command.svg_file != "")
      scene.push_back(new SvgSceneElement(command.getTween(), QString::fromStdString(command.svg_file)));
  }
}

int main(int argc, char **argv)
{
  QGuiApplication app(argc, argv);
  QRect screenGeometry = QGuiApplication::primaryScreen()->geometry();

  OverlayWindow window(screenGeometry);
  QSurfaceFormat fmt;
  //fmt.setDepthBufferSize(24);
  //fmt.setStencilBufferSize(8);
  fmt.setAlphaBufferSize(8);
  fmt.setProfile(QSurfaceFormat::CoreProfile);
  fmt.setSwapInterval(1); // the default, still 30fps...?
  QSurfaceFormat::setDefaultFormat(fmt);
  window.setFormat(fmt);
  window.setFlag(Qt::WindowTransparentForInput);
  window.setFlag(Qt::WindowDoesNotAcceptFocus);
  window.setFlag(Qt::MaximizeUsingFullscreenGeometryHint);
  window.setFlag(Qt::WindowStaysOnTopHint);

  fiber command_fiber([&](){ window.command_handler();});

  thread command_server_thread(CommandServer(), 9112, &command_channel);

  window.show();

  return app.exec();
}

#include "main.moc"
