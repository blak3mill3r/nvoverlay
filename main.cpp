#include <QOpenGLWindow>
#include <QSvgRenderer>

#include <QScreen>
#include <QPainter>
#include <QGuiApplication>
#include <QMatrix4x4>
#include <QStaticText>
#include <QKeyEvent>
#include <QElapsedTimer>
#include <QtMath>

#include <thread>

#include <boost/ptr_container/ptr_list.hpp>


#include "tweeny.h"
#include "nvoverlay.h"
#include "server.hpp"
#include "animation/SceneGraph.hpp"

#include <nlohmann/json.hpp>

using json = nlohmann::json;
using tweeny::easing;

using namespace std;
using namespace boost;
using namespace boost::fibers;
using namespace boost::this_fiber;

/*
static QPainterPath painterPathForTriangle()
{
  static const QPointF bottomLeft(-1.0, -1.0);
  static const QPointF top(0.0, 1.0);
  static const QPointF bottomRight(1.0, -1.0);

  QPainterPath path(bottomLeft);
  path.lineTo(top);
  path.lineTo(bottomRight);
  path.closeSubpath();
  return path;
}
*/


int read_int_from_env(const std::string& name) {
  int port;
  try {
    port = std::stoi(getenv(name.c_str()));
  } catch (const std::exception& e) {
    std::ostringstream ss;
    ss << "The environment variable " << name << " must be set to an integer";
    throw std::invalid_argument(ss.str());
  }
  return port;
}

int server_port() {
  return read_int_from_env("NVOVERLAY_PORT");
}

int overlay_width() {
  return read_int_from_env("NVOVERLAY_WIDTH");
}

int overlay_height() {
  return read_int_from_env("NVOVERLAY_HEIGHT");
}


class OverlayWindow : public QOpenGLWindow
{
  Q_OBJECT

public:
  OverlayWindow(QRect);
  void command_handler();

protected:
  void paintGL() override;
  void resizeGL(int w, int h) override;
  bool eventFilter(QObject *,QEvent *) override;
  //  void keyPressEvent(QKeyEvent *e) override;

private:

  void setAnimating(bool enabled);

  QMatrix4x4 m_window_painter_matrix;
  QMatrix4x4 m_projection;
  QMatrix4x4 m_view;

  FragmentToy m_fragment_toy;
  bool m_animate;

  QElapsedTimer timer;

  qint64 last_frame_time;

  SceneNode scene_root;
};

// Use NoPartialUpdate. This means that all the rendering goes directly to
// the window surface, no additional framebuffer object stands in the
// middle. This is fine since we will clear the entire framebuffer on each
// paint. Under the hood this means that the behavior is equivalent to the
// manual makeCurrent - perform OpenGL calls - swapBuffers loop that is
// typical in pure QWindow-based applications.
OverlayWindow::OverlayWindow(QRect)
  : QOpenGLWindow(QOpenGLWindow::NoPartialUpdate)
  , m_fragment_toy("./background.frag")
  , m_animate(true)
  , scene_root()
{
  // width and height are set by environment variables
  // because, nv-xmonad must doIgnore the overlay
  // managing it in any way including doFloat with a rectangle was problematic
  // after significant fiddling, this solution worked the way I want:
  //   nerdvana manages nvoverlay process and sets the environment variables NVOVERLAY_WIDTH NVOVERLAY_HEIGHT
  //     when width and height change (like, xrandr changing screen config while nv-xmonad is running) then nerdvana needs to restart nvoverlay with new dimensions
  //   nv-xmonad ignores (does not manage) the overlay window
  //   these qt window flags make it function as an overlay
  //     Qt::WindowTransparentForInput
  //     Qt::WindowDoesNotAcceptFocus
  //     Qt::MaximizeUsingFullscreenGeometryHint
  //     Qt::WindowStaysOnTopHint

  setGeometry(0, 0, overlay_width(), overlay_height());

  resizeGL(width(), height());

  //std::cout << "SETTING WIDTH AND HEIGHT: " << width() << "x" << height() << std::endl;

  m_view.lookAt(QVector3D(0,0,1),
                QVector3D(0,0,0),
                QVector3D(0,1,0));

  setAnimating(m_animate);

  last_frame_time = 0;
  timer.start();

}

bool OverlayWindow::eventFilter(QObject *, QEvent *)
{
  return false;
}

command_channel_t command_channel{16};

void OverlayWindow::paintGL()
{
  qint64 elapsed_ms = timer.elapsed();

  // hack to keep it on top of emacs... why?
  static int dong = 0;
  if(dong++ % 10 == 0) raise();

  glDisable(GL_STENCIL_TEST);
  glDisable(GL_DEPTH_TEST);

  glClearColor(0, 0, 0, 0);
  glClear(GL_COLOR_BUFFER_BIT);

  QPainter p(this);

  // measure frame time, step the scene and paint it

  int frame_time_ms = elapsed_ms - last_frame_time;

  QMatrix4x4 view_matrix = m_window_painter_matrix * m_view;

  scene_root.step(frame_time_ms);
  scene_root.paint(p, view_matrix);

  last_frame_time = elapsed_ms;

  // let the fiber manager have a go...
  yield();
}

void OverlayWindow::resizeGL(int w, int h)
{
  m_window_painter_matrix.setToIdentity();
  m_window_painter_matrix.translate(w / 2.0, h / 2.0);
  m_window_painter_matrix.scale(1.0, 0.8888888888888888);


  m_projection.setToIdentity();
  m_projection.perspective(45.f, qreal(w) / qreal(h), 0.1f, 100.f);
}

void OverlayWindow::setAnimating(bool enabled)
{
  if (enabled) {
    // Animate continuously, throttled by the blocking swapBuffers() call the
    // QOpenGLWindow internally executes after each paint. Once that is done
    // (frameSwapped signal is emitted), we schedule a new update. This
    // obviously assumes that the swap interval (see
    // QSurfaceFormat::setSwapInterval()) is non-zero.
    connect(this, &QOpenGLWindow::frameSwapped,
            this, QOverload<>::of(&QPaintDeviceWindow::update));
    update();
  } else {
    disconnect(this, &QOpenGLWindow::frameSwapped,
               this, QOverload<>::of(&QPaintDeviceWindow::update));
  }
}

void OverlayWindow::command_handler()
{
  json command;
  for(auto pop = command_channel.pop(command); pop == channel_op_status::success; pop = command_channel.pop(command)) {
    if(command.contains("containing") || command.contains("svg_file") || command.contains("message")) {
      auto node = CommandServer::createNodeFromJSON(command);
      if(node->getId() != 0)
        scene_root.deleteChild(node->getId());
      scene_root.addChild(node);
    } else if(command.contains("delete")) {
      scene_root.deleteChild((node_id_t)command["delete"]);
    }
  }
}

int main(int argc, char **argv)
{
  QGuiApplication app(argc, argv);
  QRect screenGeometry = QGuiApplication::primaryScreen()->geometry();

  OverlayWindow window(screenGeometry);
  QSurfaceFormat fmt;
  //fmt.setDepthBufferSize(24);
  //fmt.setStencilBufferSize(8);
  fmt.setAlphaBufferSize(8);
  fmt.setProfile(QSurfaceFormat::CoreProfile);
  fmt.setSwapInterval(1); // the default, still 30fps...?
  QSurfaceFormat::setDefaultFormat(fmt);
  window.setFormat(fmt);
  window.setFlag(Qt::WindowTransparentForInput);
  window.setFlag(Qt::WindowDoesNotAcceptFocus);
  window.setFlag(Qt::MaximizeUsingFullscreenGeometryHint);
  window.setFlag(Qt::WindowStaysOnTopHint);

  fiber command_fiber([&](){ window.command_handler();});

  //std::cout << "Setting server port = " << server_port() << std::endl;
  thread command_server_thread(CommandServer(), server_port(), &command_channel);

  window.show();

  return app.exec();
}

#include "main.moc"
