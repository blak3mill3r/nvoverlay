HEADERS += nvoverlay.h
INCLUDEPATH += deps/json/include
INCLUDEPATH += deps/tweeny/include
INCLUDEPATH += deps/cpp-httplib
SOURCES += nvoverlay.cpp
SOURCES += main.cpp
SOURCES += animation/*.cpp
SOURCES += server.cpp
LIBS += -lboost_fiber
LIBS += -lboost_context
QT += widgets
QT += svg
QMAKE_CXX=clang++
QMAKE_LINK=clang++

# failed attempt to use the c++20 standard header <format>
# QMAKE_CXXFLAGS += -std=c++2a
# QMAKE_CXXFLAGS += -stdlib=libc++
